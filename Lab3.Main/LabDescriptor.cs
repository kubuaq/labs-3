﻿using System;

namespace Lab3
{
    public struct LabDescriptor
    {
        #region P1

        public static Type I1 = typeof(Izagotuj_wode);
        public static Type I2 = typeof(Ipobierz_kawe);
        public static Type I3 = typeof(Imieszaj);

        public static Type Component = typeof(zaparzarka);

        public delegate object GetInstance(object zaparzarka);

        public static GetInstance GetInstanceOfI1 = (Component) => Component;
        public static GetInstance GetInstanceOfI2 = (Component) => Component;
        public static GetInstance GetInstanceOfI3 = (Component) => Component;
        
        #endregion

        #region P2

        public static Type Mixin = typeof(void);
        public static Type MixinFor = typeof(void);

        #endregion
    }
}
