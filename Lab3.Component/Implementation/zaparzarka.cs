﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{

    public interface Izagotuj_wode
    {
        int zagotuj_wode(int zagotowana);
        void test1();
    }
    public interface Ipobierz_kawe
    {
        int pobierz_kawe(int kawa_gotowa);
        void test2();
    }
    public interface Imieszaj
    {
       int  mieszaj(int gotowa_kawa);
       void test3();
    }
    public class zaparzarka :Izagotuj_wode, Ipobierz_kawe, Imieszaj
    {

        private int woda_w_zbiorniku = 1;
        private int kawa_w_zbiorniku = 1;
        private int ilosc_kawy = 0;
        private int pojemnik_kawa = 100;
        private int temp_zagotowana = 99;
        private int temp_pokojowa = 20;
       
        public int kawa_gotowa = 0;
        public int zagotowana=0;
        public int gotowa_kawa = 0;


        public int zagotuj_wode(int zagotowana)
        {
            if (woda_w_zbiorniku == 1)
            {
                    while(temp_pokojowa <99)
                    {
                        temp_pokojowa=temp_pokojowa+1;
                    }
                    if (temp_pokojowa == temp_zagotowana)
                    {
                        zagotowana = 1;
                    }
                    else
                    {
                        zagotowana = 0;
                    }
                }
            else
            {
                woda_w_zbiorniku = 0;
            }
            return zagotowana;
            }
    

        public int pobierz_kawe(int kawa_gotowa)
        {
            if (kawa_w_zbiorniku == 1)
            {
                while (ilosc_kawy < 100)
                {
                    ilosc_kawy++;
                }
                if (ilosc_kawy == pojemnik_kawa)
                {
                   kawa_gotowa = 1;
                }
                else
                {
                   kawa_gotowa = 0;
                }
            }
            else
            {
                kawa_w_zbiorniku = 0;
            }
               
            
            return kawa_gotowa;
        }
        public int mieszaj(int gotowa_kawa)
        {
            pobierz_kawe(kawa_gotowa);
            zagotuj_wode(zagotowana);
            if (zagotowana == 1 && kawa_gotowa == 1)
            {
                gotowa_kawa = 1;
            }
            else
            {
                gotowa_kawa = 0;
            }
            return gotowa_kawa;
        }
        public void test1() { }
        public void test2() { }
        public void test3() { }

        private zaparzarka kawa;

        public zaparzarka() {
        this.kawa = new zaparzarka();
        }

    }
}
